import typing

def counter(n: int, name: str) -> typing.Generator[None, None, None]:
    count = 0
    while count < n:
        yield
        print(f"{name}: {count}")
        count += 1

T = typing.TypeVar("T")

class Future(typing.Generic[T]):
    _result: typing.Union[T, None]
    def __init__(self):
        self._result = None
        self._done = False

    def done(self, value: T):
        self._result = value
        self._done = True

    def get_result(self) -> typing.Generator[None, None, T]:
        while self._result is None:
            yield
        return self._result


def task(coro: typing.Generator[None, None, T], f: Future[T]) -> typing.Generator[None, None, None]:
    result: T = yield from coro
    f.done(result)


def create_task(coro: typing.Generator[None, None, T]) -> typing.Generator[None, None, T]:
    f: Future[T] = Future()
    tasks.append(task(coro, f))
    return f.get_result()


def twice_counter() -> typing.Generator[None, None, None]:
    t1 = create_task(counter(10, "counter1"))
    t2 = create_task(counter(5, "counter2"))
    yield from t1
    yield from t2


tasks: typing.List[typing.Generator[None, None, None]] = [twice_counter()]
removing: typing.Set[typing.Generator[None, None, None]] = set()
while True:
    for t in tasks:
        try:
            t.send(None)
        except StopIteration:
            removing.add(t)
        except Exception as e:
            print(e)
            removing.add(t)
    for t in removing:
        tasks.remove(t)
    removing = set()
