"""multithreadなechoサーバー
"""
import socket
import threading

sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)

def accept(conn: socket.socket):
    try:
        data = conn.recv(1000)
        while data:
            conn.send(data)
            data = conn.recv(1000)
    finally:
        conn.close()


while True:
    conn, _ = sock.accept()
    threading.Thread(target=accept, args=(conn,)).start()
