---
title: async/await の向こう側
author: Atsushi Odagiri
date: 2022-01-22
subtitle: PyCon Kyushu 2022 Kumamoto
header-includes:
- \usepackage{luatexja}
- \usepackage[ipaex]{luatexja-preset}
theme: "Berlin"
colortheme: "beetle"
---

# はじめに

## お前誰よ

- aodag
- Atsushi Odagiri
- 株式会社オープンコレクター
- python1.5とかから

![](./r-penta512.png){width=100px}
![](./oc-logo.svg){width=100px}

## アジェンダ

- asyncioとコルーチン
- コルーチン
- 非同期IO

# async/awaitとasyncio

- Python 3.4 asyncio導入
- Python 3.5 async/await構文導入

## asyncio

- 非同期IOをコルーチンで扱えるライブラリ
- コルーチンを扱うための関数も用意されている

## async/await

- コルーチンを扱うための構文
- awaitでサブコルーチンを呼び出して大きなコルーチンを作る
- awaitにはサブコルーチンが必要
- awaitだけでコルーチンを作ることができない
- `__await__` スペシャルメソッドでawaitableな型を定義

# コルーチンとは？
## 並列処理

- 並列処理(concurrent)
  - プリエンプティブなスレッド
  - コルーチン(協調スレッド)

## プリエンプティブな並列処理

- 主にOSの機能を使う
  - geventのようにユーザーランドのみで行う実装もある
- コンテキストの切り替えは実行中のプログラムが意識することなく行われる
  - いつ切り替わるかわからないので共有リソースの扱いに注意
- IO待ちにる場合なども自動で行われるように作られている

## 協調スレッドによる並列処理

- 協調スレッド
- 自分で処理を明け渡す
   - `yield` を使う

## どうしてジェネレーターでコルーチンが作れるのか？

- `yield` で処理を明け渡す
- `send` されると値を受け取って処理を再開する
- `yield from` でコルーチンの結果を取得

## ジェネレーターでコルーチンを書く

```{python}
def counter(n: int, name: str) -> typing.Generator[None, None, None]:
    count = 0
    while count < n:
        yield
        print(f"{name}: {count}")
        count += 1
```

## ジェネレーターで書いたコルーチンを動かす
```{python}
coroutines = [counter(10, "counter1"), counter(5, "counter2")]

while coroutines:
    for c in coroutines:
        try:
            c.send(None)
        except StopIteration:
            coroutines.remove(c)
```

## コルーチンを動かしてみた結果

```
counter1: 0
counter2: 0
counter1: 1
counter2: 1
counter1: 2
counter2: 2
counter1: 3
counter2: 3
counter1: 4
counter2: 4
counter1: 5
counter1: 6
counter1: 7
counter1: 8
counter1: 9
```

## yield from で結果を取得

- `yield from` でサブコルーチンを呼ぶ
- サブコルーチンまで含めて大きなコルーチンになる
- サブコルーチンが `return` した値が `yield from` で返ってくる

## サブコルーチンをyield fromで呼ぶ

```{python}
def twice_counter() -> typing.Generator[None, None, None]:
    yield from counter(10, "counter1")
    yield from counter(5, "counter2")
```

## yield式って結局なに？

-  `next` したときにジェネレーターとしての要素を返す
-  次に `next` されるまで処理が止まる
- 値を渡すときは `send` を使う

## await vs yield from

- `await` サブコルーチンを起動して、結果を受け取る
- `yield from` サブジェネレータを起動して、ジェネレータの要素とする + 結果を受け取る
- `await` は途中経過を気にしないようになって、よりコルーチンとしての機能に特化している

# タスク!future!コルーチンを利用するためのツール

## イベントループ

- イベントについては後述
- コルーチンはジェネレーターなのでsendしてくれるループが必要
- 複数のコルーチンを順番に少しずつsendすることでそれぞれのコルーチンが実行される

## イベントループの素朴な実装

- 非同期IOに関する処理

```{python}
tasks: typing.List[typing.Generator[None, None, None]] = []
removing: typing.Set[typing.Generator[None, None, None]] = set()
while True:
    for t in tasks:
        try:
            t.send(None)
        except StopIteration:
            removing.add(t)
        except Exception as e:
            print(e)
            removing.add(t)
    for t in removing:
        tasks.remove(t)
    removing = set()
```

## futureで非同期に結果を得る

- 結果が入る場所を用意してコルーチンに渡す
- 他のコルーチンで後から結果を確認

## Futureの素朴な実装

```{python}
class Future(typing.Generic[T]):
    _result: typing.Union[T, None]
    def __init__(self):
        self._result = None
        self._done = False

    def done(self, value: T):
        self._result = value
        self._done = True

    def get_result(self) -> typing.Generator[None, None, T]:
        while self._result is None:
            yield
        return self._result
```

## コルーチンとfutureをまとめてタスクにする

- タスクは受け取ったコルーチンをサブコルーチン呼び出して結果をfutureに入れるコルーチン
- 並列で実行するようにタスクをイベントループに追加

## タスクの素朴な実装

```{python}
def task(coro: typing.Generator[None, None, T], f: Future[T]) -> typing.Generator[None, None, None]:
    result: T = yield from coro
    f.done(result)


def create_task(coro: typing.Generator[None, None, T]) -> typing.Generator[None, None, T]:
    f: Future[T] = Future()
    tasks.append(task(coro, f))
    return f.get_result()
```

## タスクを使った非同期処理

```{python}
def twice_counter() -> typing.Generator[None, None, None]:
    t1 = create_task(counter(10, "counter1"))
    t2 = create_task(counter(5, "counter2"))
    yield from t1
    yield from t2
```

# 非同期IO

- 非同期IOとはなにか
- IO待ちしている間はCPUは暇
- IO待ちしている間に別のタスクを実行したい
- IOの状況が変わるまで放置

## イベントループのイベントとは？

- IOが読み取り可能になった
- IOが書き込み可能になった

## 非同期IO（コールバック）

- コールバックでIOを待つ
- イベント発生時にコールバックを呼ぶ
- コールバックが呼ばれるまで他の処理ができる

## selectorsモジュール
- 非同期IO
  - select/pollなどの標準的な仕組み
  - kequeue, epoll, IOCPのようなOSごとの仕組み
- selectorsモジュールは様々なOS向けの非同期IOの仕組みを共通のインターフェイスで使えるようにしたラッパー

## selectorsモジュールを使う処理

- 監視対象を登録する `register`
- ポーリングしてイベントを取得 `select`
- 監視を解除 `unregister`

## 非同期IOをコルーチンで処理する

- イベントループ内でポーリングする

## 素朴なechoサーバー

- acceptしたらそのままecho処理を行う
- 1つのクライアントの処理が終わらないと他のクライアントの接続を処理できない

```{python}
sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)
while True:
    conn, addr = sock.accept()
    while True:
        data = conn.recv(1000)
        if data:
            conn.send(data)
        else:
            conn.close()
            break
```

## マルチスレッドなechoサーバー

- acceptしたら子スレッドを作成してクライアントソケットを渡す

```{python}
def accept(conn: socket.socket):
    try:
        data = conn.recv(1000)
        while data:
            conn.send(data)
            data = conn.recv(1000)
    finally:
        conn.close()

while True:
    conn, _ = sock.accept()
    threading.Thread(target=accept, args=(conn,)).start()
```

## スレッドプールを使うechoサーバー

- acceptしたらクライアントソケットをスレッドプールに渡す

```{python}
executor = ThreadPoolExecutor()

while True:
    conn, _ = sock.accept()
    executor.submit(accept, conn)

```

## selectorにコールバックを使ってechoサーバー

- コールバック処理

```{python}

def accept(sock, _):
    conn, addr = sock.accept()  # Should be ready
    print('accepted', conn, 'from', addr)
    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, read)

def read(conn, _):
    data = conn.recv(1000)  # Should be ready
    if data:
        print('echoing', repr(data), 'to', conn)
        conn.send(data)  # Hope it won't block
    else:
        print('closing', conn)
        sel.unregister(conn)
        conn.close()
```
## selectorにコールバックを使ってechoサーバー

- selectorやsocketの準備

```{python}
sel = selectors.DefaultSelector()
sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)
sock.setblocking(False)
sel.register(sock, selectors.EVENT_READ, accept)

while True:
    events = sel.select()
    for key, mask in events:
        callback = key.data
        callback(key.fileobj, mask)

```
## selectorをコルーチンで呼び出せるようにしてechoサーバー(1)

- recvをコルーチンで扱えるようにする

```{python}
def read_async(conn:socket.socket):
    f: Future[bytes] = Future()
    def callback(conn: socket.socket, _):
        sel.unregister(conn)
        f.done(conn.recv(1000))
    sel.register(conn, selectors.EVENT_READ, callback)
    return f.get_result()
```

## selectorをコルーチンで呼び出せるようにしてechoサーバー(2)

- sendをコルーチンで扱えるようにする

```{python}
def send_async(conn:socket.socket, data: bytes):
    f: Future[int] = Future()
    def callback(conn: socket.socket, _):
        sel.unregister(conn)
        f.done(conn.send(data))
    sel.register(conn, selectors.EVENT_WRITE, callback)
    return f.get_result()

```

## selectorをコルーチンで呼び出せるようにしてechoサーバー(3)

- echoタスク

```{python}
def accept(sock, _):
    conn, _ = sock.accept()
    conn.setblocking(False)
    create_task(read(conn))

def read(conn: socket.socket):
    while True:
        data = yield from read_async(conn)
        if data:
            yield from send_async(conn, data)
        else:
            break
    conn.close()
```

## selectorをコルーチンで呼び出せるようにしてechoサーバー(4)

- selectorやソケットの準備

```{python}
sel = selectors.DefaultSelector()
tasks: typing.List[typing.Generator[None, None, None]] = []

sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)
sock.setblocking(False)
sel.register(sock, selectors.EVENT_READ, accept)

```

## selectorをコルーチンで呼び出せるようにしてechoサーバー(5)

- イベントループ

```{python}
while True:
    events = sel.select()
    for key, mask in events:
        callback = key.data
        callback(key.fileobj, mask)
    removing: typing.Set[typing.Generator[None, None, None]] = set()
    for t in tasks:
        try:
            t.send(None)
        except StopIteration:
            removing.add(t)
        except Exception as e:
            print(e)
            removing.add(t)
    for t in removing:
        tasks.remove(t)
```

# おまけ

## コルーチンでないawaitable

``` {.python}
class AwaitableSample:
    def __await__(self):
        yield
        return "hello"

```

## コルーチンでない！

-   タスクが受け取るのはコルーチンであって `awaitable`
    全般ではないらしい

``` {.python}
asyncio.create_task(AwaitableSample())
```

``` {.example}
TypeError: a coroutine was expected,
got <__main__.AwaitableSample object at 0x7f4429c36100>
```


# まとめ
## まとめ
- コルーチン
  -  yieldで処理を明け渡す協調スレッド(コルーチン)
  -  `yield from` はコルーチンを起動して終了を待機する
  -  タスクは起動したコルーチンと結果を受け取る `Future` の組
- 非同期IO
  - OSごとに異なる方法でIOの状態を監視
  - ポーリングして状態チェック
  - クライアント接続ごとにスレッドを消費しない
  - 状態が変化するまでコルーチンで待つ

## 参考文献

- [PEP 255 -- Simple Generators](https://www.python.org/dev/peps/pep-0255/)
- [PEP 380 -- Syntax for Delegating to a Subgenerator](https://www.python.org/dev/peps/pep-0380/)
-[ PEP 492 -- Coroutines with async and await syntax](https://www.python.org/dev/peps/pep-0492/)
- [PEP 3156 -- Asynchronous IO Support Rebooted: the "asyncio" Module](https://www.python.org/dev/peps/pep-3156/)
- [Python3.4当時のasyncioリファレンス](https://docs.python.org/3.4/library/asyncio.html)
