"""コルーチンを使ったechoサーバー
"""
import typing
import selectors
import socket

T = typing.TypeVar("T")
sel = selectors.DefaultSelector()
tasks: typing.List[typing.Generator[None, None, None]] = []

class Future(typing.Generic[T]):
    _result: typing.Union[T, None]
    def __init__(self):
        self._result = None
        self._done = False

    def done(self, value: T):
        self._result = value
        self._done = True

    def get_result(self) -> typing.Generator[None, None, T]:
        while self._result is None:
            yield
        return self._result


def task(coro: typing.Generator[None, None, T], f: Future[T]) -> typing.Generator[None, None, None]:
    result: T = yield from coro
    f.done(result)


def create_task(coro: typing.Generator[None, None, T]) -> typing.Generator[None, None, T]:
    f: Future[T] = Future()
    tasks.append(task(coro, f))
    return f.get_result()


def read_async(conn:socket.socket):
    f: Future[bytes] = Future()
    def callback(conn: socket.socket, _):
        sel.unregister(conn)
        f.done(conn.recv(1000))
    sel.register(conn, selectors.EVENT_READ, callback)
    return f.get_result()

def send_async(conn:socket.socket, data: bytes):
    f: Future[int] = Future()
    def callback(conn: socket.socket, _):
        sel.unregister(conn)
        f.done(conn.send(data))
    sel.register(conn, selectors.EVENT_WRITE, callback)
    return f.get_result()

def accept(sock, _):
    conn, addr = sock.accept()  # Should be ready
    print('accepted', conn, 'from', addr)
    conn.setblocking(False)
    create_task(read(conn))

def read(conn: socket.socket):
    while True:
        data = yield from read_async(conn)
        if data:
            print('echoing', repr(data), 'to', conn)
            yield from send_async(conn, data)
        else:
            break
    print('closing', conn)
    conn.close()

sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)
sock.setblocking(False)
sel.register(sock, selectors.EVENT_READ, accept)

while True:
    events = sel.select()
    for key, mask in events:
        callback = key.data
        callback(key.fileobj, mask)
    removing: typing.Set[typing.Generator[None, None, None]] = set()
    for t in tasks:
        try:
            t.send(None)
        except StopIteration:
            removing.add(t)
        except Exception as e:
            print(e)
            removing.add(t)
    for t in removing:
        tasks.remove(t)
