"""スレッドプールを使うechoサーバー
"""
import socket
from concurrent.futures import ThreadPoolExecutor

sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)
executor = ThreadPoolExecutor()

def accept(conn: socket.socket):
    try:
        data = conn.recv(1000)
        while data:
            conn.send(data)
            data = conn.recv(1000)
    finally:
        conn.close()


while True:
    conn, _ = sock.accept()
    executor.submit(accept, conn)
