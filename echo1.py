"""素朴なechoサーバー
"""
import socket

sock = socket.socket()
sock.bind(('localhost', 1234))
sock.listen(100)
while True:
    conn, addr = sock.accept()
    print('accepted', conn, 'from', addr)
    while True:
        data = conn.recv(1000)  # Should be ready
        if data:
            print('echoing', repr(data), 'to', conn)
            conn.send(data)  # Hope it won't block
        else:
            print('closing', conn)
            conn.close()
            break
