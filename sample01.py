import asyncio


class AwaitableSample:
    def __await__(self):
        yield
        return "hello"


async def main():
    print(await AwaitableSample())
    done, pending = await asyncio.wait({AwaitableSample()})
    print(done)
    print(pending)
    t = asyncio.create_task(asyncio.wait({AwaitableSample()}))
    print(t)
    print(await t)

if __name__ == "__main__":
    asyncio.run(main())
