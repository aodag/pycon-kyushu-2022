all: slide.pdf

slide.pdf: slide.md
	pandoc -o $@ --pdf-engine=lualatex --slide-level=2 -s -V documentclass=beamer -V classoption=unicode -t beamer $<
