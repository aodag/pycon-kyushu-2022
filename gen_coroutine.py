import typing


def counter(n: int, name: str) -> typing.Generator[None, None, None]:
    count = 0
    while count < n:
        yield
        print(f"{name}: {count}")
        count += 1


def twice_counter() -> typing.Generator[None, None, None]:
    yield from counter(10, "counter1")
    yield from counter(5, "counter2")

coroutines = [counter(10, "counter1"), counter(5, "counter2")]

while coroutines:
    for c in coroutines:
        try:
            c.send(None)
        except StopIteration:
            coroutines.remove(c)

        
