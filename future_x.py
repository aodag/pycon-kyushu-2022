_marker = object()

class Future:
    def __init__(self):
        self._result = _marker

    def done(self, value):
        self._result = value

    def get_result(self):
        while self._result is _marker:
            yield
        return self._result


def ping(f):
    for i in range(10):
        print(i)
        s = yield
        print(s)
    f.done("done")
    yield
    for i in range(10):
        print(i)
        yield

def pong(f):
    result = yield from f.get_result()
    print(result)


f = Future()
tasks = [ping(f), pong(f)]
for t in tasks:
    try:
        t.send(None)
    except Exception as e:
        print(e)
        tasks.remove(t)
while tasks:
    for t in tasks:
        try:
            t.send(None)
        except Exception as e:
            if not isinstance(e, StopIteration):
                print(e)
            tasks.remove(t)

